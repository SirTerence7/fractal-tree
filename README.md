# Fractal Tree

the typical Fractaltree

middle Mouse button to set the angle,
't' to change between the inner and complete Tree
'u' to toggle snapping to the grid (for easier angles)
'i'/'I' for fewer / more Iterations
'U' to switch between only last and all iterations
'c' to toggle interaction
'C' to toggle smoothness of Clock
' ' to toggle Clock (clockhands[minute&second] make the Fractal, updates every second or 100 times a second[smooth])
