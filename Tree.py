import numpy as np
import matplotlib.pyplot as plt
import pylab as pl
from matplotlib import collections  as mc
from collections import deque
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
import datetime
from threading import Timer

class Tree:
    def __init__(self, alpha: float = np.deg2rad(45), beta: float = np.deg2rad(-45), iterations: int = 10, multiplier: float = 1/np.sqrt(2)):
        self.alpha = alpha
        self.beta = beta
        self.Snap = False
        self.Play = False
        self.Smooth = False
        self.only_edges = False
        self.iterations = iterations
        self.multiplier = multiplier
        self.length = 2
        self.lines = []
        self.lc = mc.LineCollection(self.lines, linewidths=1, color = 'black')
        ax1.add_collection(self.lc)
        self.draw()

    def draw(self):
        self.lines = []
        To_do = deque()
        pos_rot = np.array([[np.cos(self.alpha), -np.sin(self.alpha)], [np.sin(self.alpha), np.cos(self.alpha)]])
        neg_rot = np.array([[np.cos(self.beta), -np.sin(self.beta)], [np.sin(self.beta), np.cos(self.beta)]])
        To_do.append([np.array([0,-self.length]), np.array([0,self.length]), 0])
        while len(To_do) != 0:
            [start, vector, iteration] = To_do.popleft()
            end = np.add(start,vector)
            if iteration < iterations:
                if not self.only_edges:
                    self.lines.append([(start[0], start[1]), (end[0], end[1])])
                To_do.append([end, np.dot(pos_rot, vector)*multiplier,iteration+1])
                To_do.append([end, np.dot(neg_rot, vector)*multiplier,iteration+1])
            else:
                self.lines.append([(start[0], start[1]), (end[0], end[1])])

        self.lc.set_segments(self.lines)
        fig1.canvas.draw()

    def play(self):
        if not self.Play:
            return
        if self.Smooth:
            player= Timer(0.01, self.play)
            #player.daemon=True
            self.alpha = np.deg2rad((datetime.datetime.now().minute + datetime.datetime.now().second/60) * -6)
            self.beta = np.deg2rad((datetime.datetime.now().second + datetime.datetime.now().microsecond/1000000) * -6)
        else:
            player= Timer(1.0, self.play)
            #player.daemon=True
            self.alpha = np.deg2rad((datetime.datetime.now().minute + datetime.datetime.now().second/60) * -6)
            self.beta = np.deg2rad((datetime.datetime.now().second) * -6)
        self.draw()
        player.start()


def get_rad(a: np.ndarray, b: np.ndarray = np.array([0, 1])):
    if a[0] < 0:
        return np.arccos(np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b)))
    else:
        return -np.arccos(np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b)))

def draw_lines(x_val: np.float64, y_val: np.float64):
    theta = get_rad(np.array([x_val, y_val]))
    rot = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
    x = [0,0]
    y = [-start_length,0]
    temp_v = np.dot(rot,np.array([0,start_length*multiplier]))
    temp_v = np.array([-temp_v[0], temp_v[1]])
    x.append(x[-1]+temp_v[0])
    y.append(y[-1]+temp_v[1])
    for i in range(iterations):
        temp_v = np.dot(rot,temp_v)*multiplier
        x.append(x[i+2]+temp_v[0])
        y.append(y[i+2]+temp_v[1])
    x = [-a for a in x[::-1]]+x
    y = y[::-1]+y
    line.set_data(x,y)
    fig1.canvas.draw()

def update(x,y):
    if Snap:
        x = np.round(x*2)/2
        y = np.round(y*2)/2
    if Tree_draw:
        my_tree.draw()
    else: 
        draw_lines(x,y)

def motion_notify_callback(event):
    if not Change:
        return
    if not event.inaxes:
        return
    if event.xdata == None or event.ydata == None:
        return
    if event.button == 1:
        if my_tree.Snap:
            event.xdata = np.round(event.xdata*2)/2
            event.ydata = np.round(event.ydata*2)/2
        my_tree.alpha = get_rad(np.array([event.xdata, event.ydata]))
    elif event.button == 3:
        if my_tree.Snap:
            event.xdata = np.round(event.xdata*2)/2
            event.ydata = np.round(event.ydata*2)/2
        my_tree.beta = get_rad(np.array([event.xdata, event.ydata]))
    elif event.button != 2:
        return
    update(event.xdata, event.ydata)

def key_press(event):
    if not event.inaxes:
        return
    global iterations
    if event.key == 't':
        global Tree_draw
        Tree_draw = not Tree_draw
        if Tree_draw:
            line.set_data(0,0)
        else:
           my_tree.lc.set_segments([])
    elif event.key == 'I':
        iterations = min(25, iterations+1)
        my_tree.iterations = min(25, my_tree.iterations+1)
    elif event.key == 'i':
        iterations = max(1, iterations-1)
        my_tree.iterations = max(1, my_tree.iterations-1)
    elif event.key == 'u':
        global Snap
        my_tree.Snap = not my_tree.Snap
        Snap = not Snap
    elif event.key == 'U':
        my_tree.only_edges = not my_tree.only_edges
    elif event.key == 'c':
        global Change
        Change = not Change
        return
    elif event.key == 'C':
        my_tree.Smooth = not my_tree.Smooth
        return
    elif event.key == ' ':
        my_tree.Play = not my_tree.Play
        if my_tree.Play:
            my_tree.play()
        return
    else:
        return
    update(event.xdata, event.ydata)

fig1 = plt.figure(figsize=(11, 11))  # instantiate a figure to draw
ax1 = plt.axes()  # create an axes object
ax1.set_xlim([-4.5,4.5])
ax1.set_ylim([-4,5])

start_length = 2
multiplier = 1/np.sqrt(2)
iterations = 11
Tree_draw = True
Change = True
Snap = False
Smooth = False

my_tree = Tree()
line = ax1.plot([0,0], [-4,0], linewidth=1, color = 'black')[0]

fig1.canvas.mpl_connect('motion_notify_event', motion_notify_callback)
fig1.canvas.mpl_connect('key_press_event', key_press)

ax1.xaxis.set_major_locator(MultipleLocator(0.5))
ax1.yaxis.set_major_locator(MultipleLocator(0.5))
ax1.xaxis.set_minor_locator(AutoMinorLocator(4))
ax1.yaxis.set_minor_locator(AutoMinorLocator(4))
ax1.grid(which='both')
ax1.grid(which='minor', alpha=0.2)
ax1.grid(which='major', alpha=0.5)
plt.show()